FROM python:3.10-slim-bullseye

#install security updates
RUN apt-get update && apt-get -y upgrade && apt-get -y install gcc libpq-dev

# fill out the rest of the Dockerfile to package and run demo_app
# demo_app is a Django app. Dependencies are in requirements.txt

WORKDIR /usr/src/demo_project

COPY . .

RUN pip install -r requirements.txt

RUN cd demo_app && ./manage.py migrate && ./manage.py createsuperuser

EXPOSE 8000

CMD ["python", "manage", "runserver", "0.0.0.0:8000"]